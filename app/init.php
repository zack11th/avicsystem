<?php

// основные настройки
ini_set('default_charset', 'UTF-8');
date_default_timezone_set('UTC');
set_include_path('.');


// обработка ошибок
error_reporting(-1);
ini_set('log_errors', 0);
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
// задали константу APP_ROOT = корневое имя папки нашего сайта (monstruum)
define('APP_ROOT', dirname(__DIR__));