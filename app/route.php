

<?php

define('APP_CONRTOLLER', APP_ROOT . '/controller');
define('APP_VIEW', APP_ROOT . '/views');

//задаем переменную url которая берет то, что пользователь ввел в браузере
$url = $_SERVER['REQUEST_URI'];
//делаем из url массив, который будет отсекать шлак после знака ? если вдруг пользователь попытается добавить что-то после него
$url = explode('?', $url)[0];
//создаём переменную controller, которая будет играть роль нашего index.php в папке controller и открывать его, когда пользователь просто введёт адрес сайта
$controller = $url;
if ($url === '/') {
    $controller = 'index';
}

$file = APP_CONRTOLLER . '/' . $controller . '.php';

if (!file_exists($file)) {
    $controller = null;
}
//защищаемся от того, что контроллера нет
if (!$controller) {
    require (APP_CONRTOLLER . '/error.php');
}else {
    require ($file);
}

