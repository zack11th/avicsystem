<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo($title)?></title>
    <link href="img/label.png" rel="shortcut icon" type="image/x-icon"> <!--иконка-->
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<div id="header">
    <div class="container head-container">
        <div class="big_cellphone">
            <?php require (APP_VIEW . '/tel.php') ?>
        </div>
        <div id="label">
            <a href="/">
                <img id="label-pic" src="img/label.svg" alt="">
                <img id="avic" src="img/AVIC.png" alt="AVIC System">
            </a>
        </div>
        <div id="up_up">
            <p>Автономная система водоснабжения и канализации<br>
                Продажа и установка оборудования, доставка воды</p>
        </div>
        <div class="grig-cont">
            <div id="sityarea">
                <div class="sity"><p>Омск</p></div>
                <div class="sity"><p>Новосибирск</p></div>
            </div>
            <input type="checkbox" name="toggle" id="mobMenu" class="mobMenu">
            <label for="mobMenu" class="mobMenu"><i class="fa fa-bars"></i></label>
            <ul id="topmenu">
                <li><a href="/" class="topmenu_bottom hide">главная</a></li>
                <li><a href="/about" class="topmenu_bottom">наши приемущества</a></li>
                <li><a href="/contact" class="topmenu_bottom">контакты</a></li>
            </ul>
        </div>
    </div>
</div><!-- /#header -->
<div id="wrapper">
    <?php require (APP_VIEW . '/' . $view . '.php') ?>
</div><!-- /#wrapper -->
<div id="footer">
    <div class="container grid-footer">
        <div>
            <div class="uslugi">Услуги предоставляются:</div>
            <div class="company">ООО AVIC System <br>
                г. Омск, 2018 год</div>
        </div>
        <div class="see_as">Смотрите так же на <br>сайте: </div>
        <div class="ul-footer">
            <ul>
                <li><a href="/about">Наши <br>приемущества</a></li>
                <li><a href="/contact">Контакты</a></li>
            </ul>
        </div>
        <div class="copyright">&copy; webstudio 2018 год</div>
    </div>
</div><!--/#footer-->
<div class="form-wrapper">
    <input type="checkbox" name="toggle" id="show-form" class="toggleForm">
    <div class="form-container">
        <label for="show-form" class="toggleForm">ЗАКАЗАТЬ</label>
        <form class="formBox" id="form">
            <div class="inputBox">
                <label class="labelText">Ваше имя*:</label>
                <input type="text" name="username" id="name" class="userInput" required>
            </div>
            <div class="inputBox">
                <label class="labelText">Номер телефона (только числа)*:</label>
                <input type="tel" name="userphone" id="userphone" class="userInput" required pattern="\d+">
            </div>
            <div class="inputBox">
                <label class="labelText">Сообщение:</label>
                <textarea name="message" id="message" class="userInput"></textarea>
            </div>
            <div class="personalDoc">
                <input type="checkbox" required>
                <label>Я согласен на обработку моих <a href="http://www.kremlin.ru/acts/bank/24154" target="_blank">персональных данных</a></label>
            </div>
            <input type="submit" id="send" value="Отправить" class="form-button">
            <div class="mail-send">
                <p><b>Спасибо за заявку!</b><br>Мы Вам в скором времени перезвоним.</p>
                <label for="show-form" class="form-button" id="ok-send-mail">Ok</label>
            </div>
        </form>

    </div>
</div><!--/form-wrapper-->
<script src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="slick/slick.min.js"></script>
<script type="text/javascript" src="js/js.js"></script>
<script type="text/javascript" src="js/galery.js"></script>
</body>
</html>