<div class="container error-cont">
    <h1>Упс!..Запрашиваемая Вами страница не найдена</h1>
    <p>Возможно эта страница находится на стадии наполнения или вовсе не существует.</p>
    <p>В любом случае, Вы всегда можете связаться с нами по телефону <b><?php require (APP_VIEW . '/tel.php') ?></b> и узнать всю информацию "из первых рук"<br>
        Так же Вы можете перейти <a href="/"><b>на главную страницу</b></a> и узнать всю основную информацию там.</p>
    <div class="btn-to-home">
        <a href="/" class="button-to-home"><b>НА ГЛАВНУЮ</b></a>
    </div>
</div>