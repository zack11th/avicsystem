<div class="container">
    <div class="article contact-container">
        <h4>Как заказать АВиК</h4>
        <img src="img/ask.png" alt="">
        <p>Пишите нам или звоните по телефону/связывайтесь через WhatsApp</p>
        <div class="contact-inf">
            <p class="mail mail-cont"><b>avicsystem@mail.ru</b></p>
            <div class="phone phone-cont">
                <div class="watsapp">
                    <a href="https://api.whatsapp.com/send?phone=<?php require (APP_VIEW . '/tel_whatsapp.php') ?>" target="_blank"></a>
                </div>
                <p><b><?php require (APP_VIEW . '/tel.php') ?></b></p>
            </div>
        </div>
        <form class="formBox cont-formBox" id="form-big">
            <div class="inputBox">
                <label class="labelText">Ваше имя*:</label>
                <input type="text" name="username" id="name" class="userInput" required>
            </div>
            <div class="inputBox">
                <label class="labelText">Номер телефона (только числа)*:</label>
                <input type="tel" name="userphone" id="userphone" class="userInput" required pattern="\d+">
            </div>
            <div class="inputBox">
                <label class="labelText">Сообщение:</label>
                <textarea name="message" id="message" class="userInput"></textarea>
            </div>
            <div class="personalDoc">
                <input type="checkbox" required>
                <label>Я согласен на обработку моих <a href="http://www.kremlin.ru/acts/bank/24154" target="_blank">персональных данных</a></label>
            </div>
            <input type="submit" id="send-big" value="Отправить" class="form-button">
            <div class="mail-send cont-mail-send">
                <p><b>Спасибо за заявку!</b><br>Мы Вам в скором времени перезвоним.</p>
                <label class="form-button" id="ok-send-mail-big">Ok</label>
            </div>
        </form>
        <div class="btn-to-home">
            <a href="/" class="button-to-home"><b>НА ГЛАВНУЮ</b></a>
        </div>
<!--        <div style="height: 50px"></div>-->
    </div>
</div>