$(document).ready(function () {

// slider
$('#head_slider').slick({
    autoplay: true,
    arrows: false,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    pauseOnFocus: false,
    pauseOnHover: false,
    lazyLoad: 'progressive',
    zIndex: 0,
    draggable: false
});

// form-focus
$(".userInput").focus(function() {
    $(this).parent().addClass("focus-input");
}).blur(function() {
    if($(this).val()===''){
        $(this).parent().removeClass("focus-input");
    }
});

// обратная связь
    $("#form").submit(function () {
        var clears = this;
        $.ajax({
            type: "POST",
            url: "ajax/mail.php",
            data: $(this).serialize()
        }).done(function () {
            $(".mail-send").show();
            clears.reset();
        });
        return false;

    });
});
//Закрывает окно благодарности, после нажатия на ок
$("#ok-send-mail").click(function () {
    $(".mail-send").hide();
});

// обратная связь большой формы
$("#form-big").submit(function () {
    var clears = this;
    $.ajax({
        type: "POST",
        url: "ajax/mail.php",
        data: $(this).serialize()
    }).done(function () {
        $(".mail-send").show();
        clears.reset();
    });
    return false;

});
//Закрывает окно благодарности, после нажатия на ок
$("#ok-send-mail-big").click(function () {
    $(".mail-send").hide();
});